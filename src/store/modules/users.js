import api from '../../api';

const state = {
	filtro: '',
	users: []
}

const getters = {
	allUsers: state => {
		if ( state.filtro === '' )
			return state.users
		else
			return state.users.filter( u => u.name.fullname.toLowerCase().includes( state.filtro.toLowerCase() ) )
	}
}

const actions = {
	async loadUsers ( { commit } ) {
		const response = await api.get( '/users' )
		commit( 'setUsers', response.data )
	},
	setFiltro ( { commit }, filtro ) {
		commit( 'setFiltro', filtro )
	}
}

const mutations = {
	setUsers : ( state, users ) => state.users = users,
	setFiltro: ( state, filtro ) => state.filtro = filtro
}

export default {
	state,
	getters,
	actions,
	mutations
}
